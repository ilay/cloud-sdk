FROM google/cloud-sdk:alpine

# Install OpenSSL
RUN apk upgrade --update-cache --available && \
    apk add openssl && \
    rm -rf /var/cache/apk/*

# Install Kubectl
RUN gcloud components install kubectl

# Install Helm
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
